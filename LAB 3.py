import random as rd

''''
#Escribir un programa en PYTHON que utilice una función lambda para sumar
#dos valores y multiplicarlos un tercer valor ingresado por el usuario,
#el programa debe realizar la operación hasta que el usuario indique
#lo contrario.

opcion = "n"

while opcion != "s":
    valor1 = int(input("Ingrese un numero: "))
    valor2 = int(input("Ingrese otro numero: "))
    valor3 = int(input("Ingrese otro numero: "))
    z = 0
    def procedimiento(x, y, z):
        return (x+y)*z
    b = lambda x, y, z: [x + y]*z
    print(b)
    print("Desea agregar más valores (s/n): ")
'''

''''
#Escribir en Python un programa que permita agregar valores a una lista y a un diccionario
#según sea la elección del usuario, el programa deberá continuar agregando elementos
#hasta que el usuario lo decida y cuando este finaliza de realizar la inserción de elementos, el
#programa deberá imprimir los elementos almacenados en ambas estructuras
#Los keys del diccionario deben ser generados aleatoriamente utilizando la función random
#Los índices de la lista deben empezar con el número 35 en adelante.


lista = list()
diccionario1 = dict()

opcion = "s"
option1 = input("Desea agregar elementos a las estructuras: (s/n) \n"
                    "S = Si \n"
                    "N = No ")
while opcion != "n":
    elemento = input("Lista o Diccionario (L-D): \n"
                     "L = Lista \n"
                     "D = Diccionario \n")

    if elemento == "L":
        insertar = int(input("Agregue un elemento: "))
        lista.append(insertar)
        insertar2 = input("Desea agregar otro elemento: (s/n) ")

        if insertar2 == "s":
            continue
        else:
            break

    elif elemento == "D":
        key1 =rd.randint(1,30)
        value1 = int(input("Digite un valor: "))
        diccionario1={key1:value1}
        key2 = input("Desea agregar otro elemento: (s/n) ")

        if elemento == "s":
            continue
        else:
            break
print("Los elementos de la lista son: ")
for i, x in enumerate(lista, start=35):
    print("El indice es: ",i, "y el valor es: ",x )
print("------------------------------------")
print("Los elementos del diccionario son: ")
for key in (diccionario1):
    print(key," - ", diccionario1[key])
'''

''''
#Programar una clase en python que reciba un texto cualquiera ingresado por el usuario y lo
#imprima de forma inversa.

class Texto:
    palabra = input("Digite una palabra: ")
    text = palabra[::-1]
    print(text)
'''

#Escribir una clase en python llamada CalculaFigura que pueda devolver por medio de dos
#métodos (calcular_area y calcular_perímetro), el área y el perímetro de tres figuras
#diferentes (triangulo equilatero, cuadrado,circulo) . 25 puntos
#1. La selección de la figura es ingresada por el usuario, así como los valores
#correspondientes a la fórmula de cada figura
#2. La clase debe tener un contructor con un atributo TEXTO que imprima por defecto un
#saludo al usuario
#3. El programada debe calcular el área o el perímetro de las figuras hasta que el usuario
#yo no desee realizar más cálculos.


figura = input("Digite la figura: \n"
                   "T: Triangulo \n"
                   "O: Circulo \n"
                   "C: Cuadrado")
operacion = input("Indique lo que desea calcular: \n"
                  "A: Area \n"
                  "P: Perimetro")

class CalculaFigura:
    def calcular_area(self, p_figura, p_calcular):
        self.figura = p_figura
        self.calcular = p_calcular
        if figura == "T":
            print("Ingrese el valor de la base: ")
            base = int(input("\t\t\t\tBase en cm:"))
            print("Ingrese el valor de la altura: ")
            altura= int(input("\t\t\t\tAltura en cm: "))
            area2 =base*altura/2
            print("El area es: ",area2)

        if figura == "O":
            radio = int(input("Ingrese el valor del radio: "))
            pi = 3,14
            area2 = pi*radio**2
            print("El area es: ",area2)

        if figura == "C":
            lado = int(input("Ingrese el valor del lado: "))
            area2= lado*lado
            print("El area es: ", area2)

    def calcular_perimetro(self, p_figura, p_calcular):
        self.figura = p_figura
        self.calcular = p_calcular
        if figura == "T":
            lado1 = int(input("Ingrese el valor del primer lado:"))
            lado2 = int(input("Ingrese el valor del segundo lado: "))
            lado3 = int(input("Ingrese el valor del tercer lado:"))
            perimetro = lado1+lado2+lado3
            print("El perimetro es: ",perimetro)

        if figura == "O":
            radio = int(input("Ingrese el valor del radio: "))
            pi = 3,14
            perimetro = 2*pi*radio
            print("La longitud de la circunferencia es: ",perimetro)

        if figura == "C":
            lado = int(input("Ingrese el valor del lado: "))
            perimetro = 4*lado
            print("El perimetro es: ", perimetro)


objetoNuevo = CalculaFigura

opcion = "s"

def menu():
    while opcion != "n":
        print("1: Area \n"
              "2: Perimetro \n"
              "3: Salir del programa ")

        if opcion == 1:
            print()















